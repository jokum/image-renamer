﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace Image_Renamer
{
    using System.Security;
    using Extensions;

    /*
     * This is the form the user sees.
     */
    public partial class Program : Form
    {
        private ICollection<string> unchanged;

        private string[] extensions = { ".jpg", ".jpeg", ".png", ".bmp", ".gif" };

        [STAThread]
        static void Main(string[] args)
        {
            Application.Run(new Program());
        }

        public Program()
        {
            InitializeComponent();

            renameList.AllowDrop = true;
            renameList.DragOver += new System.Windows.Forms.DragEventHandler(this.Form_DragOver);
            renameList.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form_DragDrop);

            ignoreList.AllowDrop = true;
            ignoreList.DragOver += new System.Windows.Forms.DragEventHandler(this.Form_DragOver);
            ignoreList.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form_DragDrop);

            ctxMenuItem_Remove.Click += new System.EventHandler(this.menuItem_Click);
            ctxMenuItem_Remove.Enabled = false;
        }

        #region Drag/drop handlers.
        private void Form_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] paths = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string path in paths)
                {
                    // Drag/drop only for images and directories.
                    FileInfo f = new FileInfo(path);
                    if (Directory.Exists(path) || f.Exists && extensions.Contains(f.Extension))
                    {
                        e.Effect = DragDropEffects.Copy;
                    }
                }
            }
        }

        private void Form_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            string[] paths = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string path in paths)
            {
                if (((ListBox)sender).FindString(path) == ListBox.NoMatches &&
                    (Directory.Exists(path) || File.Exists(path)))
                {
                    ((ListBox)sender).Items.Add(path);
                    go.Enabled = renameList.Items.Count > 0;
                    progressBar.Value = 0;
                    ctxMenuItem_Remove.Enabled = true;
                }
            }
        }
        #endregion

        private void menuItem_Click(object sender, EventArgs e)
        {
            ListBox lb = ctxMenuStrip.SourceControl as ListBox;
            if (lb != null)
            {
                if (lb.SelectedItems.Count > 0)
                {
                    for (int i = lb.Items.Count - 1; i >= 0; --i)
                    {
                        if (lb.SelectedItems.Contains(lb.Items[i]))
                        {
                            lb.Items.RemoveAt(i);
                        }
                    }
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void go_Click(object sender, EventArgs e)
        {
            // Don't want to risk calling this method twice at the same time.
            go.Enabled = false;
            unchanged = new List<string>();

            List<FileInfo> files = new List<FileInfo>();
            foreach (string path in renameList.Items)
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                if (dir.Exists)
                {
                    files.AddRange(dir.GetFilesByExtensions(extensions));
                }
                else if (File.Exists(path))
                {
                    files.Add(new FileInfo(path));
                }
            }

            progressBar.Maximum = files.Count;
            foreach (FileInfo f in files)
            {
                Rename(f, f.DirectoryName + "/" + GetFileHash(f.FullName) + f.Extension);
            }
            renameList.Items.Clear();
            ctxMenuItem_Remove.Enabled = false;
            
            // If some files weren't renamed because of a conflict the info label
            // is changed. Change it back when all is right again.
            if (unchanged.Count == 0)
            {
                info.Text = "Drag files and folders to the top window to rename all"
                    + Environment.NewLine
                    + "found image files according to their MD5 hash values,"
                    + Environment.NewLine
                    + "skipping all paths in the ignore list.";
            }
            else
            {
                renameList.Items.AddRange(unchanged.ToArray());
            }
            go.Enabled = renameList.Items.Count > 0;
        }

        /*
         * Tries to rename file f to name.
         */
        private void Rename(FileInfo f, string name)
        {
            progressBar.PerformStep();

            // If the canonical path has a prefix in the ignore list this file
            // shouldn't be renamed.
            foreach (string ignore in ignoreList.Items)
            {
                if (f.FullName.StartsWith(ignore))
                {
                    return;
                }
            }

            if (f.Name != name)
            {
                try
                {
                    f.MoveTo(name);
                }
                catch (FileNotFoundException)
                {
                    MessageBox.Show("Could not locate" + Environment.NewLine + f.FullName, "Error!");
                }
                catch (IOException)
                {
                    if (File.Exists(name) && f.Exists)
                    {
                        unchanged.Add(name);
                        unchanged.Add(f.FullName);
                        info.Text = "Some files were not renamed because of naming conflicts."
                            + Environment.NewLine
                            + "They have been automatically added again in an order"
                            + Environment.NewLine
                            + "that should resolve the conflict. If not, there may be talk of"
                            + Environment.NewLine
                            + "a hash collision that must be resolved manually.";
                    }
                    else
                    {
                        MessageBox.Show("Error trying to rename" + Environment.NewLine + f.FullName
                            + Environment.NewLine + "Target device may not be ready.", "Error!");
                    }
                }
                catch (SecurityException se)
                {
                    MessageBox.Show(se.Message, "Error!");
                }
                catch (Exception boo)
                {
                    MessageBox.Show("You managed to produce an unanticipated exception: " + boo.Message, "Oops!");
                }
            }
        }

        /*
         * Computes the MD5 hash of the file pointed to by path.
         */
        public static string GetFileHash(string path)
        {
            StringBuilder sb = new StringBuilder();
            MD5 hasher = MD5.Create();
            using (FileStream fs = File.OpenRead(path))
            {
                foreach (Byte b in hasher.ComputeHash(fs))
                {
                    sb.Append(b.ToString("x2").ToLower());
                }
            }

            return sb.ToString();
        }
    }
}


namespace Extensions
{
    public static class Extension
    {
        /*
         * Return only those files whose extensions match the argument.
         */
        public static IEnumerable<FileInfo> GetFilesByExtensions(this DirectoryInfo dirInfo, params string[] extensions)
        {
            var allowedExtensions = new HashSet<string>(extensions, StringComparer.OrdinalIgnoreCase);

            return dirInfo.EnumerateFiles("*", SearchOption.AllDirectories).Where(f => allowedExtensions.Contains(f.Extension));
        }
    }
}