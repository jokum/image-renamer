﻿namespace Image_Renamer
{
    partial class Program
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.go = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.renameList = new System.Windows.Forms.ListBox();
            this.ctxMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxMenuItem_Remove = new System.Windows.Forms.ToolStripMenuItem();
            this.info = new System.Windows.Forms.Label();
            this.ignoreList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.ctxMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // go
            // 
            this.go.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.go.Enabled = false;
            this.go.Location = new System.Drawing.Point(396, 380);
            this.go.Name = "go";
            this.go.Size = new System.Drawing.Size(75, 23);
            this.go.TabIndex = 1;
            this.go.Text = "Rename";
            this.go.UseVisualStyleBackColor = true;
            this.go.Click += new System.EventHandler(this.go_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(315, 380);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // renameList
            // 
            this.renameList.AllowDrop = true;
            this.renameList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.renameList.ContextMenuStrip = this.ctxMenuStrip;
            this.renameList.Cursor = System.Windows.Forms.Cursors.Default;
            this.renameList.Location = new System.Drawing.Point(12, 26);
            this.renameList.Name = "renameList";
            this.renameList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.renameList.Size = new System.Drawing.Size(459, 173);
            this.renameList.Sorted = true;
            this.renameList.TabIndex = 3;
            // 
            // contextMenuStrip1
            // 
            this.ctxMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxMenuItem_Remove});
            this.ctxMenuStrip.Name = "contextMenuStrip1";
            this.ctxMenuStrip.Size = new System.Drawing.Size(142, 26);
            // 
            // rawrToolStripMenuItem
            // 
            this.ctxMenuItem_Remove.Enabled = false;
            this.ctxMenuItem_Remove.Name = "rawrToolStripMenuItem";
            this.ctxMenuItem_Remove.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.ctxMenuItem_Remove.Size = new System.Drawing.Size(141, 22);
            this.ctxMenuItem_Remove.Text = "Remove";
            // 
            // info
            // 
            this.info.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.info.AutoSize = true;
            this.info.Location = new System.Drawing.Point(9, 348);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(260, 39);
            this.info.TabIndex = 4;
            this.info.Text = "Drag files and folders to the top window to rename all\r\nfound image files accordi" +
    "ng to their MD5 hash values,\r\nskipping all paths in the ignore list.";
            // 
            // ignoreList
            // 
            this.ignoreList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ignoreList.ContextMenuStrip = this.ctxMenuStrip;
            this.ignoreList.FormattingEnabled = true;
            this.ignoreList.Location = new System.Drawing.Point(12, 238);
            this.ignoreList.Name = "ignoreList";
            this.ignoreList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.ignoreList.Size = new System.Drawing.Size(459, 108);
            this.ignoreList.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(227, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Rename List: drag files/folders to rename here.";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(211, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Ignore List: drag files/folders to ignore here.";
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(315, 351);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(155, 23);
            this.progressBar.Step = 1;
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 8;
            // 
            // Program
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(484, 415);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.info);
            this.Controls.Add(this.renameList);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.go);
            this.Controls.Add(this.ignoreList);
            this.Name = "Program";
            this.Text = "Image MD5 Renamer";
            this.ctxMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button go;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ListBox renameList;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.ListBox ignoreList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.ContextMenuStrip ctxMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem ctxMenuItem_Remove;
    }
}